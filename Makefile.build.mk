REVISION := $(shell git rev-parse --short HEAD || echo unknown)
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
LAST_TAG := $(shell git describe --tags --abbrev=0)
LATEST_STABLE_TAG := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)
COMMITS := $(shell echo `git log --oneline $(LAST_TAG)..HEAD | wc -l`)
VERSION := $(shell cat VERSION)
CI_REGISTRY ?= registry.gitlab.com/gitlab-org/release-cli

# default to CGO_ENABLED=0 but can be overriden
CGO_ENABLED := $(if $(CGO_ENABLED),$(CGO_ENABLED),0)

ifneq (v$(VERSION),$(LAST_TAG))
	VERSION := $(shell echo $(VERSION)~beta.$(COMMITS).g$(REVISION))
endif

LD_FLAGS := '-X "main.VERSION=$(VERSION)" -s -w'

_allpackages = $(shell (go list ./...))

# memoize allpackages, so that it's executed only once and only if used
allpackages = $(if $(__allpackages),,$(eval __allpackages := $$(_allpackages)))$(__allpackages)

.PHONY: setup build run clean
setup: clean
	$Q mkdir -p bin/
	$Q mkdir -p out/cover/ out/junit/
# From https://marcofranssen.nl/manage-go-tools-via-go-modules/
	@cat cmd/release-cli/tools.go | \
		grep _ | \
		awk -F'"' '{print $$2}' | \
		GOBIN=$(CURDIR)/bin xargs -tI % go install %

build: setup
	echo "VERSION: $(VERSION)"
	echo $(allpackages)
	$Q go build -ldflags $(LD_FLAGS) -o bin/$(PROJECT_NAME) ./cmd/$(PROJECT_NAME)

docker:
	$Q docker build -t ${CI_REGISTRY}:${REVISION} .

run: setup
	$Q go run cmd/$(PROJECT_NAME)/main.go

clean:
	$Q rm -rf bin/*
	$Q rm -rf out/*

BUILD_PLATFORMS ?= -os '!netbsd' -os '!openbsd'

build-binaries: setup
	# Building $(PROJECT_NAME) in version $(VERSION) for $(BUILD_PLATFORMS)
	${CURDIR}/bin/gox $(BUILD_PLATFORMS) \
		-ldflags $(LD_FLAGS) \
		-output="bin/binaries/$(PROJECT_NAME)-{{.OS}}-{{.Arch}}" \
		./cmd/$(PROJECT_NAME)

.PHONY: release_s3
release_s3: CI_COMMIT_REF_NAME ?= $(BRANCH)
release_s3: CI_COMMIT_SHA ?= $(REVISION)
release_s3: S3_BUCKET ?=
release_s3:
	@$(MAKE) prepare_index
ifneq ($(S3_BUCKET),)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)"
ifeq ($(shell git describe --exact-match --match $(LATEST_STABLE_TAG) >/dev/null 2>&1; echo $$?), 0)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/latest";
endif
ifeq ($(CI_COMMIT_REF_NAME), $(CI_DEFAULT_BRANCH))
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/edge";
endif
endif

.PHONY: sync_s3_release
sync_s3_release: S3_URL ?=
sync_s3_release:
	# Syncing with $(S3_URL)
	@aws s3 sync bin/binaries/ "$(S3_URL)" --acl public-read
	@echo "==> Download index file: https://$$S3_BUCKET.s3.amazonaws.com/$$CI_COMMIT_REF_NAME/index.html"

RELEASE_INDEX_GEN_VERSION ?= latest
RELEASE_INDEX_GENERATOR ?= .tmp/release-index-gen-$(RELEASE_INDEX_GEN_VERSION)

prepare_index: export CI_COMMIT_REF_NAME ?= $(BRANCH)
prepare_index: export CI_COMMIT_SHA ?= $(REVISION)
prepare_index: $(RELEASE_INDEX_GENERATOR)
	# Preparing index file
	@$(RELEASE_INDEX_GENERATOR) -working-directory bin/binaries/ \
								-project-version $(VERSION) \
								-project-git-ref $(CI_COMMIT_REF_NAME) \
								-project-git-revision $(CI_COMMIT_SHA) \
								-project-name "GitLab Release CLI" \
								-project-repo-url "https://gitlab.com/gitlab-org/release-cli" \
								-gpg-key-env GPG_KEY \
								-gpg-password-env GPG_PASSPHRASE

$(RELEASE_INDEX_GENERATOR): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(RELEASE_INDEX_GENERATOR): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/release-index-generator/$(RELEASE_INDEX_GEN_VERSION)/release-index-gen-$(OS_TYPE)-amd64"
$(RELEASE_INDEX_GENERATOR):
	# Installing $(DOWNLOAD_URL) as $(RELEASE_INDEX_GENERATOR)
	@mkdir -p $(shell dirname $(RELEASE_INDEX_GENERATOR))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(RELEASE_INDEX_GENERATOR)"
	@chmod +x "$(RELEASE_INDEX_GENERATOR)"

.PHONY: remove_s3_release
remove_s3_release: CI_COMMIT_REF_NAME ?= $(BRANCH)
remove_s3_release: S3_BUCKET ?=
remove_s3_release:
	@echo "The S3 Bucket: $(S3_BUCKET)"
ifneq ($(S3_BUCKET),)
	@aws s3 rm "s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)" --recursive
endif
